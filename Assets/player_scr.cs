﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine.Experimental.Networking;

public class player_scr : MonoBehaviour {
    
    // +++ private fields +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
	private int[] values = new int[10];      // value array that will store the last received values
	private int runner;                     // runner field use to cycle through the array
	public Text debugText;                  // reference to the text field  




    // +++ unity standard methods +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    void Start ()
	{
		StartCoroutine(upm_grabber());
        debugText.text = "Start upm query...";
	}




    // +++ custom methods +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    IEnumerator upm_grabber()
    {
		// init www grabber
		WWW www = null;

        // endless loop
		while (true)
        { 
            // query sensor 5 times a second
			yield return new WaitForSeconds(0.1f);
            www = new WWW ("http://192.168.2.115:8082");
            yield return www;

			if (!string.IsNullOrEmpty (www.error)) {
				Debug.Log (www.error);
				continue;
			}

            //Debug.Log(www.text);
            // parse int from json
            Debug.Log(www.text);
            int upm = ParseValueFromResponse(www.text);

            // output value
            debugText.text = upm.ToString();
        }
    }

    /// <summary>
    /// Parses the upm value from http response.
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    private int ParseValueFromResponse(string text)
    {
        // parse json object
        JSONObject j = new JSONObject(text);

        // fill array with values
        IncrementRunner();
        values[runner % values.Length] = (int)j["payload"]["upm"].i;

        // calculate avarage upm
        int upm = 60000 / (calcAvg() * 3);

        return upm / 5*5;
    }

    /// <summary>
    /// This method builds up a runner variable, that is used to cycle through
    /// the array.
    /// </summary>
    private void IncrementRunner()
    {
        runner++;
        if (runner == int.MaxValue)
            runner = 0;
        
    }

    /// <summary>
    /// This method calculates the average about the values stored in the array.
    /// </summary>
    /// <returns>The average upm</returns>
    private int calcAvg(){

		int sum = 0;

        for (int i = 0, n = values.Length; i < n; i++)
        {
            //Debug.Log(i.ToString() + ": " + values[i]);
            sum += values[i];
        }

        return sum / values.Length;
	}
}
