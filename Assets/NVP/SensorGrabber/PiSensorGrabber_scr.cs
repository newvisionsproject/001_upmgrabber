﻿using System;
using UnityEngine;
using System.Collections;

public class PiSensorGrabber_scr : MonoBehaviour
{

    public static int UPM = 0;

    [SerializeField] private string _upmIp;
    [SerializeField] private int _upmPort;
    [SerializeField] private float _queryInterval;
    [SerializeField] private int _numEventsPerTurn;
    [SerializeField] private int _intervallRange;
    [SerializeField] private int _numOfValuesForAverage;

    private string _upmUrl;
    private int[] valueArray;
    private int runner;                     // runner field use to cycle through the array

    // Use this for initialization
    void Start ()
	{

	    _upmUrl = "http://" + _upmIp + ":" + _upmPort;
        Debug.Log("Upm Url: " + _upmUrl);

        valueArray = new int[_numOfValuesForAverage];
	    for (int i = 0; i < _numOfValuesForAverage; i++)
	    {
	        valueArray[i] = Int32.MaxValue;
	    }

	    StartCoroutine(GrabUpm());

	}

    IEnumerator GrabUpm()
    {
        while (true)
        {
            
            WWW www = new WWW(_upmUrl);
            yield return www;

            if (!string.IsNullOrEmpty(www.error))
            {
                Debug.Log(www.error);
                continue;
            }

            UPM = ParseTextResponse(www.text);

            yield return new WaitForSeconds(_queryInterval);
        }
    }

    private int ParseTextResponse(string responseText)
    {
        int index = responseText.IndexOf(":", StringComparison.Ordinal)+1;
        responseText = responseText.Substring(index, responseText.Length - index);
        //Debug.Log(responseText);
        int value = Convert.ToInt32(responseText);

        IncrementRunner();
        valueArray[runner%valueArray.Length] = value;

        int upm = 60000 / (calcAvg() * _numEventsPerTurn);

        return upm / _intervallRange * _intervallRange;
    }

    /// <summary>
    /// This method calculates the average about the values stored in the array.
    /// </summary>
    /// <returns>The average upm</returns>
    private int calcAvg()
    {

        int sum = 0;

        for (int i = 0, n = valueArray.Length; i < n; i++)
        {
            //Debug.Log(i.ToString() + ": " + values[i]);
            sum += valueArray[i];
        }

        return sum / valueArray.Length;
    }

    /// <summary>
    /// This method builds up a runner variable, that is used to cycle through
    /// the array.
    /// </summary>
    private void IncrementRunner()
    {
        runner++;
        if (runner == int.MaxValue)
            runner = 0;

    }

    /// <summary>
    /// Parses the upm value from http response.
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    private int ParseValueFromResponse(string text)
    {
        /*
        // json objects from asset store is required
        // parse json object
        JSONObject j = new JSONObject(text);

        // fill array with values
        IncrementRunner();
        values[runner % values.Length] = (int)j["payload"]["upm"].i;

        // calculate avarage upm
        int upm = 60000 / (calcAvg() * 3);

        return upm / 5 * 5;
        */
        return 0;
    }
}
